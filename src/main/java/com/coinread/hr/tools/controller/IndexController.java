package com.coinread.hr.tools.controller;

import com.coinread.hr.tools.service.UserService;
import com.coinread.hr.tools.utils.CookieUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("index")
public class IndexController {

    @Autowired
    private UserService userService;

    @RequestMapping("")
    public ModelAndView index(){
        return new ModelAndView("index");
    }

    @RequestMapping("login")
    public ModelAndView login(){
        return new ModelAndView("login");
    }

    @RequestMapping("login/check")
    public ModelAndView loginCheck(String user,String pwd){
        int res = 0 ;
        try {
            res = userService.login(user,pwd);
        }catch (Exception e){
            ModelAndView model = new ModelAndView("alert");
            model.addObject("message","用户名或密码错误！");
            return model;
        }
        if (res==1) {
            return new ModelAndView("redirect:/index");
        }
        ModelAndView model = new ModelAndView("alert");
        model.addObject("message","用户名或密码错误！");
        return model;
    }
}
