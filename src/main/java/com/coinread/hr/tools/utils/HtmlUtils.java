package com.coinread.hr.tools.utils;

import com.coinread.hr.tools.bean.Info;

import java.util.Map;

public class HtmlUtils {

    public static String genHtmlMailContent(Info info, String title, Map<Integer,String> map){
        StringBuilder sb = new StringBuilder();
        sb.append("<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "\t<title>"+title+"</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<p>Dear"+genDearName(info.getUser().getName())+"，</p>\n" +
                "\n" +
                "<p>下表是您本月工资明细，请查收。</p>\n" +
                "\n" +
                "<table border=\"1\" bordercolor=\"#a0c6e5\" style=\"border-collapse:collapse;\">\n" +
                "\t<tr>\n");
        for (Map.Entry<Integer,String> entry:map.entrySet()){
            sb.append("\t\t<th>"+entry.getValue()+"</th>\t\n");
        }
        sb.append("\t</tr>\n" +
                "\t<tr>\n");
        for (Map.Entry<Integer,String> entry:info.getInfo().entrySet()){
            sb.append("\t\t<td>"+entry.getValue()+"</td>\t\n");
        }
        sb.append("\t</tr>\n" +
                "</table>\n" +
                "<p>—————————————</p>\n" +
                "<p>币读</p>\n" +
                "<p>人力资源部</p>\n" +
                "\n" +
                "</body>\n" +
                "</html>");
        return sb.toString();
    }

    public static String genHtmlVerifyContent(String message, String title){
        StringBuilder sb = new StringBuilder();
        sb.append("<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "\t<title>"+title+"</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "\n" +
                "<p>温馨提示：</p>\n" +
                "\n");
        sb.append("<p>"+message+"</p>");
        sb.append("<p>—————————————</p>\n" +
                "<p>币读</p>\n" +
                "\n" +
                "</body>\n" +
                "</html>");
        return sb.toString();
    }

    public static String genDearName(String name){
        if (name.length()>=3)
            name = name.substring(1,name.length());
        return name;
    }
}
