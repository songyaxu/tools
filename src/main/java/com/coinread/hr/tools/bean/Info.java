package com.coinread.hr.tools.bean;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class Info {
    private User user;
    private Map<Integer,String> info;
}
