package com.coinread.hr.tools.bean.persistence;

import com.coinread.hr.tools.bean.Info;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Slf4j
@Service
public class UserPersistence {

    private volatile List<Info> info = Lists.newArrayList();

    @PostConstruct
    public void init(){
        this.info=Lists.newArrayList();
    }

    public void add(Info info){
        this.info.add(info);
    }

    public void setInfo(List<Info> info) {
        this.info = info;
    }

    public List<Info> getInfo() {
        return info;
    }

    public int size(){
        return info.size();
    }

    public void printf(){
        this.info.forEach(info1 -> {
            log.info("用户 = {}",info1.getInfo().toString());
        });
    }
}
