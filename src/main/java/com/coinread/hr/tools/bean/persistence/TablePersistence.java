package com.coinread.hr.tools.bean.persistence;

import com.google.common.collect.Lists;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Service
public class TablePersistence {

    private volatile Map<Integer,String> tableNameList= new HashMap<>();

    @PostConstruct
    public void init(){
        this.tableNameList = new HashMap<>();
    }

    public void setTableNameList(Map<Integer, String> tableNameList) {
        this.tableNameList = tableNameList;
    }

    public Map<Integer, String> getTableNameList() {
        return tableNameList;
    }

    public void add(Integer key,String value){
            this.tableNameList.put(key,value);
    }

    public int size(){
        return this.tableNameList.size();
    }

    public Integer getName(){
        for (int i = 0 ;i<20;i++) {
         String value = this.tableNameList.get(i);
         if (value.equals("姓名"))
             return i;
        }
        return null;
    }

    public Integer getEmail(){
        for (int i = 0 ;i<20;i++) {
            String value = this.tableNameList.get(i);
            if (value.equals("邮箱"))
                return i;
        }
        return null;
    }
}
