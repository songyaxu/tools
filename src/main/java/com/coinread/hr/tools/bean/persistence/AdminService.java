package com.coinread.hr.tools.bean.persistence;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author ：yaxuSong
 * @Description:
 * @Date: 14:58 2018/6/25
 * @Modified by:
 */
@Service
public class AdminService {
    private volatile String pass= new String("");

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
}
