package com.coinread.hr.tools.config;

import com.coinread.hr.tools.interceptor.LoginInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Author ：yaxuSong
 * @Description:
 * @Date: 16:14 2018/6/25
 * @Modified by:
 */
@Configuration
public class MvcConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoginInterceptor()).excludePathPatterns("**/").excludePathPatterns("/index/login").excludePathPatterns("/assets/**").excludePathPatterns("/index/login/check");
    }
}
