package com.coinread.hr.tools.service.impl;

import com.coinread.hr.tools.bean.User;
import com.coinread.hr.tools.bean.persistence.AdminService;
import com.coinread.hr.tools.service.MailService;
import com.coinread.hr.tools.utils.HtmlUtils;
import com.coinread.hr.tools.utils.MailUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * @Author ：yaxuSong
 * @Description:
 * @Date: 14:47 2018/6/25
 * @Modified by:
 */
@Slf4j
@Service
public class MailServiceImpl implements MailService {

    @Override
    public int send(String messages, String title,String toMail,String sender,String pwd){
        JavaMailSender javaMailSender = MailUtils.initJavaMailSender(sender,pwd);
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        try {
            helper.setFrom(sender);
            helper.setTo(toMail);
            helper.setText(messages,true);
            helper.setSubject(title);
            javaMailSender.send(message);
            return 1;
        }catch (Exception e){
            log.error("发送失败 e = {}",e.getMessage());
        }
        return 0;
    }
}
