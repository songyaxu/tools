package com.coinread.hr.tools.service;

/**
 * @Author ：yaxuSong
 * @Description:
 * @Date: 17:39 2018/6/25
 * @Modified by:
 */
public interface UserService {

    int login(String user,String pwd) throws Exception;
}
