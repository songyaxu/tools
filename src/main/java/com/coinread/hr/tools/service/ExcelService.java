package com.coinread.hr.tools.service;

import com.coinread.hr.tools.bean.Info;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ExcelService {

    void getList(MultipartFile file);

    void setNull();
}
