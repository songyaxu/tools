package com.coinread.hr.tools.service;

import com.coinread.hr.tools.bean.User;

/**
 * @Author ：yaxuSong
 * @Description:
 * @Date: 14:47 2018/6/25
 * @Modified by:
 */
public interface MailService {

    int send(String messages, String title, String toMail,String sender,String pwd);
}
